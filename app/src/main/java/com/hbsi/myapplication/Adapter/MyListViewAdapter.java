package com.hbsi.myapplication.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.hbsi.myapplication.R;

import java.util.ArrayList;

public class MyListViewAdapter extends BaseAdapter {

    ArrayList<String> listText;
    LayoutInflater inflater;
    Context context;
    private  int current ;

    public MyListViewAdapter(Context context, ArrayList<String> listText) {
        this.context = context;
        this.listText = listText;
        inflater = LayoutInflater.from(context);

    }
    public void setCurrent(int current)
    {
        this.current = current;

    }

    @Override
    public int getCount() {
        return listText.size();
    }

    @Override
    public Object getItem(int i) {
        return listText.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        ViewHolder holder = null;
        if(view==null){
            view = inflater.inflate(R.layout.list_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }else {
           holder =  (ViewHolder)view.getTag();
        }
        holder.textView.setText(listText.get(i));

//        左側列表选中高亮
        if(current == i)
        {   view.setBackgroundResource(R.color.color3);
            holder.getTextView().setTextColor(Color.WHITE);
        }else{
            holder.getTextView().setTextColor(Color.BLACK);
            view.setBackgroundResource(R.color.color2);
        }


        return view;
    }
    class  ViewHolder{
        TextView textView;
        public  ViewHolder(View view){
            textView = view.findViewById(R.id.list_text);

        }

        public TextView getTextView() {
            return textView;
        }


    }
}
