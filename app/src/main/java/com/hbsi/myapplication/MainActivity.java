package com.hbsi.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.hbsi.myapplication.Viewpager.BlankFragment1;
import com.hbsi.myapplication.Viewpager.BlankFragment2;
import com.hbsi.myapplication.Viewpager.BlankFragment3;
import com.hbsi.myapplication.Viewpager.MyViewPagerAdpter;


import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements BlankFragment1.OnFragmentInteractionListener, BlankFragment2.OnFragmentInteractionListener, BlankFragment3.OnFragmentInteractionListener {
    Handler handler;
    public final static int TESTDEMO = 0;
    ArrayList<Fragment> arrayList;
    ViewPager viewPager;
    BottomNavigationView bottom;
    private TextView title;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewy();
        initViewPagery();
        initDongHuay();
    }

    private void initDongHuay() {
        Animation aaa = AnimationUtils.loadAnimation(this,R.anim.kaichang_anim);
        imageView.setAnimation(aaa);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setVisibility(View.INVISIBLE);
            }
        });
        aaa.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initViewPagery() {
        arrayList.add(new BlankFragment1());
        arrayList.add(new BlankFragment2());
        arrayList.add(new BlankFragment3());
        MyViewPagerAdpter adpter = new MyViewPagerAdpter(getSupportFragmentManager(), arrayList);
        viewPager.setAdapter(adpter);
        viewPager.setCurrentItem(0);
        bottom.getMenu().getItem(0).setChecked(true);
        bottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.item1:
                        viewPager.setCurrentItem(0);
                        title.setText("实时");
                        break;
                    case R.id.item2:
                        viewPager.setCurrentItem(1);
                        title.setText("历史");

                        break;
                    case R.id.item3:
                        viewPager.setCurrentItem(2);
                        title.setText("设置");

                        break;
                }
                return true;
            }
        });
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottom.getMenu().getItem(position).setChecked(true);
                switch (position) {
                    case 0:
                        title.setText("实时");
                        break;
                    case 1:
                        title.setText("历史");
                        break;
                    case 2:
                        title.setText("设置");
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initViewy() {
        imageView = findViewById(R.id.kcdh);
        viewPager = findViewById(R.id.main_viewpager);
        arrayList = new ArrayList<>();
        bottom = findViewById(R.id.main_bottom);
        title = findViewById(R.id.title);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
