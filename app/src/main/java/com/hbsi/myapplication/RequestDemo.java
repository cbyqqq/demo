package com.hbsi.myapplication;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;

import org.json.JSONObject;

import java.util.Map;

public class RequestDemo {
    public  static  void  getPost(String url, Map map, Response.Listener<JSONObject> listener,Response.ErrorListener errorListener){
        JSONObject jsonObject = new JSONObject(map);
        JsonRequest jsonRequest = new JsonObjectRequest(1, GetUrl.getPostUrl+url, jsonObject,listener,errorListener);
        MyApplication.queue.add(jsonRequest);
    }
    public  static  void  getGet(String url,Response.Listener<JSONObject> listener,Response.ErrorListener errorListener){
        JSONObject jsonObject = new JSONObject();
        JsonRequest jsonRequest = new JsonObjectRequest(0,GetUrl.getGetUrl+url,jsonObject,listener,errorListener);
        MyApplication.queue.add(jsonRequest);
    }
}
