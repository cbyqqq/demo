package com.hbsi.myapplication.Viewpager;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.hbsi.myapplication.Adapter.MyListViewAdapter;
import com.hbsi.myapplication.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment1 extends Fragment {
    private ListView listView;
    private ArrayList<Integer> listImg;
    private ArrayList<String> listText;
    private Context context;
    private MyListViewAdapter adapter;
    TextView textView11, textView12, textView13, textView14,
            textView21, textView22, textView23, textView24,
            textView31, textView32, textView33, textView34;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BlankFragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment1 newInstance(String param1, String param2) {
        BlankFragment1 fragment = new BlankFragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        listText = new ArrayList();


        listText.add("节点001");
        listText.add("节点002");
        listText.add("节点003");
        listText.add("节点004");
        listText.add("节点005");


        adapter = new MyListViewAdapter(context, listText);
        listView.setAdapter(adapter);
        initActionc();

    }

    private void initActionc() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                listView
                adapter.setCurrent(i);
//                必须刷新，否则变化
                adapter.notifyDataSetChanged();
//                if (adapterView.getTag() != null) {
//                    ((View) adapterView.getTag()).setBackgroundDrawable(null);
//                }
//                adapterView.setTag(view);
//                view.setBackgroundResource(R.color.color1);
                initActionRight(i);

            }
        });
    }

    public void initActionRight(int i) {
        switch (i) {
            case 0:
                initSetData(textView11, textView12, textView13, textView14, "35℃", "40％", "32℃", "39％");
                initSetData(textView21, textView22, textView23, textView24, "36℃", "38.5％", "35.6℃", "42％");
                initSetData(textView31, textView32, textView33, textView34, "35℃", "40％", "35℃", "37.8％");
                break;
            case 1:
                initSetData(textView11, textView12, textView13, textView14, "27℃", "50％","26℃", "35.4％");
                initSetData(textView21, textView22, textView23, textView24, "28℃", "24.5％","25℃", "45％");
                initSetData(textView31, textView32, textView33, textView34, "34℃", "35.8％","35℃", "33.6％");
                break;

            case 2:
                initSetData(textView11, textView12, textView13, textView14, "26℃", "43％","28℃", "44％");
                initSetData(textView21, textView22, textView23, textView24, "35.7℃", "38％","36℃", "42.1％");
                initSetData(textView31, textView32, textView33, textView34, "30.3℃", "40％","29℃", "39％");
                break;

            case 3:
                initSetData(textView11, textView12, textView13, textView14, "35.5℃", "40.9％","35℃", "39.5");
                initSetData(textView21, textView22, textView23, textView24, "33.6℃", "40.2％","30℃", "42％");
                initSetData(textView31, textView32, textView33, textView34, "38℃", "35％","40℃", "33％");
                break;

            case 4:
                initSetData(textView11, textView12, textView13, textView14, "27℃", "40％","25℃", "42％");
                initSetData(textView21, textView22, textView23, textView24, "29℃", "43％","30℃", "43％");
                initSetData(textView31, textView32, textView33, textView34, "24℃", "50％","20℃", "55％");
                break;

            default:
                break;
        }
    }

    private void initSetData(TextView textView11, TextView textView12, TextView textView13, TextView textView14, String a, String b, String c, String d) {
        textView11.setText(a);
        textView12.setText(b);
        textView13.setText(c);
        textView14.setText(d);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank_fragment1, container, false);

//       获取组件id
        listView = view.findViewById(R.id.list_view);
        context = getActivity().getApplicationContext();

        textView11 = view.findViewById(R.id.z1_tv1);
        textView12 = view.findViewById(R.id.z1_tv2);
        textView13 = view.findViewById(R.id.z1_tv3);
        textView14 = view.findViewById(R.id.z1_tv4);
        textView21 = view.findViewById(R.id.z2_tv1);
        textView22 = view.findViewById(R.id.z2_tv2);
        textView23 = view.findViewById(R.id.z2_tv3);
        textView24 = view.findViewById(R.id.z2_tv4);
        textView31 = view.findViewById(R.id.z3_tv1);
        textView32 = view.findViewById(R.id.z3_tv2);
        textView33 = view.findViewById(R.id.z3_tv3);
        textView34 = view.findViewById(R.id.z3_tv4);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
