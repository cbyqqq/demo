package com.hbsi.myapplication.Viewpager;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.hbsi.myapplication.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ArrayList<Entry> values1 = new ArrayList<>();//数据源1
    ArrayList<Entry> values2 = new ArrayList<>();//数据源2
    ArrayList<Entry> values3 = new ArrayList<>();//数据源1
    ArrayList<Entry> values4 = new ArrayList<>();//数据源2
    ArrayList<Entry> values5 = new ArrayList<>();//数据源1

    ArrayList<Float> Yvaluesone = new ArrayList<>();//即时形变
    ArrayList<Float> Yvaluestwo = new ArrayList<>();//累计形变
    ArrayList<Float> Yvaluesthree = new ArrayList<>();//即时形变
    ArrayList<Float> Yvaluesfour = new ArrayList<>();//累计形变
    ArrayList<Float> Yvaluesfive= new ArrayList<>();//即时形变
    LineChart lineChart;
    LineDataSet set1;


    XAxis xAxis;
    YAxis yAxis;
    private OnFragmentInteractionListener mListener;

    public BlankFragment2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BlankFragment2.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment2 newInstance(String param1, String param2) {
        BlankFragment2 fragment = new BlankFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setLineChare();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank_fragment2, container, false);


        lineChart =view.findViewById(R.id.lineChart);

        return  view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    private void setLineChare() {
        xAxis=new XAxis();

        yAxis = lineChart.getAxisLeft();
        YAxis rightYAxis = lineChart.getAxisRight();
        //设置Y轴是否显示
        rightYAxis.setEnabled(false); //右侧Y轴不显示
        xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(10);
        final String[] weekStrs = new String[]{"0h", "2h", "4h", "6h", "8h", "10h","12h","14h","16h","18h","20h","22h","24h"};

        xAxis.setDrawGridLines(false); // 不绘制网格线


// 设置标签的显示格式
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return weekStrs[(int) value];
            }
        });


        for (int i = 0; i < 13; i++) {//设置假的数据
            Yvaluesone.add((float)(-30+Math.random()*(40-(-30)+1)));
            Yvaluestwo.add((float) (0+Math.random()*(100-0+1)));
            Yvaluesthree.add((float) (0+Math.random()*(300-0+1)));
            Yvaluesfour.add((float) (0+Math.random()*(60-0+1)));
            Yvaluesfive.add((float) (1+Math.random()*(5 -1+1)));
        }
        for (int i = 0; i < Yvaluesone.size(); i++) {//把数据存转存成Entry的list
            values1.add(new Entry(i, Yvaluesone.get(i)));
        }
        for (int i = 0; i < Yvaluestwo.size(); i++) {
            values2.add(new Entry(i, Yvaluestwo.get(i)));
        }
        for (int i = 0; i < Yvaluesthree.size(); i++) {
            values3.add(new Entry(i, Yvaluesthree.get(i)));
        }
        for (int i = 0; i < Yvaluesfour.size(); i++) {
            values4.add(new Entry(i, Yvaluesfour.get(i)));
        }
        for (int i = 0; i < Yvaluesfive.size(); i++) {
            values5.add(new Entry(i, Yvaluesfive.get(i)));
        }
        //设置数据1  参数1：数据源 参数2：图例名称
        ArrayList<Integer> coloer=new ArrayList<>();




        coloer.add(Color.rgb(110,100,1));
        coloer.add(Color.rgb(220,0,100));
        coloer.add(Color.rgb(180,100,1));
        coloer.add(Color.rgb(190,220,100));
        coloer.add(Color.rgb(100,0,1));



        LineDataSet set2 = setLineDataSet(values1, "温度(℃)", coloer.get(0));
        LineDataSet set3 = setLineDataSet(values2, "湿度(%)", coloer.get(1));
        LineDataSet set4 = setLineDataSet(values3, "光照(LUX)", coloer.get(2));
        LineDataSet set5 = setLineDataSet(values4, "土壤(%)", coloer.get(3));
        LineDataSet set6 = setLineDataSet(values5, "CO2浓度(PPM)", coloer.get(4));


        LineData data = new LineData(set2,set3,set4,set5,set6);
        // 添加到图表中
        lineChart.setData(data);
        lineChart.animateY(9500);
        lineChart.animateX(9500);
        //绘制图表
        //是否展示网格线\
           lineChart.setDoubleTapToZoomEnabled(true);
        lineChart.setDrawGridBackground(true);
        lineChart.setDragEnabled(true);
        lineChart.setNoDataText("hbsi");
    }

    private  LineDataSet  setLineDataSet( ArrayList<Entry> values,String name,int colors) {
        set1 = new LineDataSet(values, name);//把Entry格式的list加到第一条折线上
        set1.setColor(colors);
        set1.setCircleColor(colors);
        set1.setValueTextColor(colors);
        set1.setLineWidth(1f);//设置线宽
        set1.setCircleRadius(3f);//设置焦点圆心的大小
        set1.enableDashedHighlightLine(10f, 5f, 0f);//点击后的高亮线的显示样式
        //set1.setHighlightLineWidth(2f);//设置点击交点后显示高亮线宽
        set1.setHighlightEnabled(true);//是否禁用点击高亮线
        set1.setHighLightColor(Color.RED);//设置点击交点后显示交高亮线的颜色
        set1.setValueTextSize(9f);//设置显示值的文字大小
        set1.setDrawFilled(false);//设置禁用范围背景填充

        return set1;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
