package com.hbsi.myapplication.Viewpager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MyViewPagerAdpter extends FragmentPagerAdapter {
    ArrayList<Fragment> arrayList;

    public MyViewPagerAdpter(FragmentManager fm, ArrayList<Fragment> fragmentlist) {
        super(fm);
        this.arrayList = fragmentlist;
    }

    @Override
    public Fragment getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }
}
